# COMP411-Lab9

More Recursion in Assembly
Due 11:55pm on Wednesday, November 6, 2019.

Exercise 1: Binary Pattern Generation
In this exercise, you will write a recursive procedure that, given a number N, generates and prints all binary patterns of length N. For example, for N=3, it will output:

000
001
010
011
100
101
110
111
Likewise, if N were 4, the output will be 16 lines long, corresponding to the binary strings of length 4, in increasing order, starting with 0000 and ending with 1111. Assume N will not be more than 20.

This exercise must be done using recursion. A non-recursive solution will receive zero credit. Moreover, you are not allowed to solve this problem by simply counting integers and printing them in binary. Your code must use a recursive procedure, each call of which sets a single character in the pattern (first to 0 and then to 1), and recursively calls the procedure to generate patterns over the remaining characters. See under Tips below.

C Version
Name your file ex1.c, and test using your own inputs as well as the input/output sample files.

Tips:

Think carefully about how to cast this problem as one of recursion. In particular, to generate all binary patterns of length N, do the following:
First, set the leftmost position to '0', and recursively generate all binary patterns over the remaining N-1 positions.
Next, set the leftmost position to '1', and again recursively generate all binary patterns over the remaining N-1 positions.
Your entire C program, including main, can be as few as 20 lines long! If your code is much longer than that, chances are that you are making it more complicated than it needs to be.
I will post a skeleton of my solution (C program only) on Saturday, November 2, morning. I strongly suggest you try your best until then to come up with an efficient C program, and then compare it with my code skeleton. If your code is much longer, you may use my code skeleton and fill in the blanks to arrive at your final code.
MIPS Assembly Version
Now, you will write MIPS assembly code to do the same. Your assembly implementation must be recursive. A non-recursive implementation will receive zero credit. Name your file ex1.asm, and test using your own inputs as well as the input/output sample files.

Tips:

The easiest way (rather, the only way!) to develop the assembly code here is to convert your C code to assembly line-by-line. Just be sure to start with correct C code that is properly recursive.
Use the procedure coding templates listed under Lab 8 to develop your main procedure as well as the recursive called procedure. Once again, you may use either the "4b" templates or the "5a" templates; my recommendation is to construct your assembly code in such a manager that the simpler "4b" templates can be used.
Test Inputs, Due Date and Submission Procedure
Sample inputs and corresponding outputs are provided on the comp411-2fa19.cs.unc.edu server under /home/students/montek/comp411/samples/lab9.

Your assignment (i.e., the files ex1.c and ex1.asm) must be submitted electronically by 11:55pm on Wednesday, November 6, 2019.

Once you are satisfied that your assembly program is running fine within MARS on your laptop, copy the file ex1.asm to the server under the appropriate folder in your home directory (e.g., comp411lab/lab9). If you have developed your C code on the server, the file ex1.c should already be in that directory, else copy it to that folder. Then, run the self-checking script:

% cd ~/comp411lab/lab9
% cp /home/students/montek/comp411/samples/lab9/* .
% selfchecklab9

How to submit: If the final version of either of your programs were edited on the server, first transfer your work back to your laptop. Next, log in to Sakai in a browser window, and look for the lab under "Assignments" in the left panel. Attach the requested files and submit.

In case of any problems, please contact the instructor or the TAs.

30 October 2019, Montek Singh, montek@cs.unc.edu
